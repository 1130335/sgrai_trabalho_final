#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <iostream>
#include <string>
#include "AudioPlayer.h"

using namespace std;

#define DEBUG               1

#define DELAY_MOVIMENTO     20
#define RAIO_ROTACAO        20

// last cursor click
int g_cursor_x = 0;
int g_cursor_y = 0;

AudioPlayer player;

/* VARIAVEIS GLOBAIS */

typedef struct {
	GLboolean up, down, right, left, i, o;
}Teclas;

typedef struct {
	GLfloat    x, y, z;
}Pos;

typedef struct {
	Pos      eye, center, up;
	GLfloat  fov;
}Camera;


typedef struct {
	GLboolean   doubleBuffer;
	GLint       delayMovimento;
	Teclas      teclas;
	GLuint      menu_id;
	GLboolean   menuActivo;
	Camera      camera;
	GLboolean   debug;
	GLboolean   ortho;
}Estado;

typedef struct {
	GLfloat     x, y;
	GLfloat		dimensaoCubos;
	GLfloat		numCubosHorizontal;
	GLfloat		numCubosVertical;
	GLfloat		alturaDamas;
}Damas;


Estado estado;
Damas tabuleiro;



/* Inicializa��o do ambiente OPENGL */
void Init(void)
{
	string audioName = "Damas.mp3";
	player.setFileName(audioName);
	player.playSoundThreaded();

	estado.debug = DEBUG;
	estado.menuActivo = GL_FALSE;
	estado.delayMovimento = DELAY_MOVIMENTO;
	estado.camera.eye.x = 40;
	estado.camera.eye.y = 0;
	estado.camera.eye.z = 40;
	estado.camera.center.x = 0;
	estado.camera.center.y = 0;
	estado.camera.center.z = 0;
	estado.camera.up.x = 0;
	estado.camera.up.y = 0;
	estado.camera.up.z = 1;
	estado.ortho = GL_TRUE;
	estado.camera.fov = 60;


	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	//glutIgnoreKeyRepeat(GL_TRUE);
	g_cursor_x = g_cursor_y = 500; // middle of window

}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint)width, (GLint)height);


	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	// projeccao ortogonal 2D, com profundidade (Z) entre -1 e 1
	if (estado.debug)
		printf("Reshape %s\n", (estado.ortho) ? "ORTHO" : "PERSPECTIVE");

	if (estado.ortho)
	{
		if (width < height)
			glOrtho(-20, 20, -20 * (GLdouble)height / width, 20 * (GLdouble)height / width, -100, 100);
		else
			glOrtho(-20 * (GLdouble)width / height, 20 * (GLdouble)width / height, -20, 20, -100, 100);
	}
	else
		gluPerspective(estado.camera.fov, (GLfloat)width / height, 1, 100);

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
}


void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat cor[])
{

	glBegin(GL_POLYGON);
	glColor3fv(cor);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
}


GLfloat azulClaro[][3] = { { 1, 0.7, 0.4 },
{ 1, 0.7, 0.4 },
{ 1, 0.7, 0.4 },
{ 1, 0.7, 0.4 },
{ 1, 0.7, 0.4 },
{ 1, 0.7, 0.4 },
{ 1, 0.7, 0.4 } };

GLfloat branco[][3] = { { 1.0, 1.0, 1.0 },
{ 1.0, 1.0, 1.0 },
{ 1.0, 1.0, 1.0 },
{ 1.0, 1.0, 1.0 },
{ 1.0, 1.0, 1.0 },
{ 1.0, 1.0, 1.0 },
{ 1.0, 1.0, 1.0 } };

GLfloat azul[][3] = { { 0.0, 0.0, 1.0 },
{ 0.0, 0.0, 1.0 },
{ 0.0, 0.0, 1.0 },
{ 0.0, 0.0, 1.0 },
{ 0.0, 0.0, 1.0 },
{ 0.0, 0.0, 1.0 },
{ 0.0, 0.0, 1.0 } };

GLfloat cinza[][3] = { { 0.5, 0.5, 0.5 },
{ 0.5, 0.5, 0.5 },
{ 0.5, 0.5, 0.5 },
{ 0.5, 0.5, 0.5 },
{ 0.5, 0.5, 0.5 },
{ 0.5, 0.5, 0.5 },
{ 0.5, 0.5, 0.5 } };

GLfloat preto[][3] = { { 0, 0, 0 },
{ 0, 0, 0 },
{ 0, 0, 0 },
{ 0, 0, 0 },
{ 0, 0, 0 },
{ 0, 0, 0 },
{ 0, 0, 0 } };


GLfloat marron[][3] = { { 0.5, 0.0, 0.05 },
{ 0.5, 0.0, 0.05 },
{ 0.5, 0.0, 0.05 },
{ 0.5, 0.0, 0.05 },
{ 0.5, 0.0, 0.05 },
{ 0.5, 0.0, 0.05 },
{ 0.5, 0.0, 0.05 } };

GLfloat castanho[][3] = { { 0.7, 0.5, 0.04 },
{ 0.7, 0.5, 0.04 },
{ 0.7, 0.5, 0.04 },
{ 0.7, 0.5, 0.04 },
{ 0.7, 0.5, 0.04 },
{ 0.7, 0.5, 0.04 },
{ 0.7, 0.5, 0.04 } };


GLfloat castanhoEscuro[][3] = { { 0.3, 0.2, 0},
{ 0.3, 0.2, 0 },
{ 0.3, 0.2, 0 },
{ 0.3, 0.2, 0 },
{ 0.3, 0.2, 0 },
{ 0.3, 0.2, 0 },
{ 0.3, 0.2, 0 } };

GLfloat beje[][3] = { { 1.0, 1.0, 0.6 },
{ 1.0, 1.0, 0.6 },
{ 1.0, 1.0, 0.6 },
{ 1.0, 1.0, 0.6 },
{ 1.0, 1.0, 0.6 },
{ 1.0, 1.0, 0.6 },
{ 1.0, 1.0, 0.6 } };

void desenhaCuboTabuleiro(GLfloat cores[][3])
{
	GLfloat vertices[][3] = { { -0.5, -0.5, -0.5 },
	{ 0.5, -0.5, -0.5 },
	{ 0.5, 0.5, -0.5 },
	{ -0.5, 0.5, -0.5 },
	{ -0.5, -0.5, 0.5 },
	{ 0.5, -0.5, 0.5 },
	{ 0.5, 0.5, 0.5 },
	{ -0.5, 0.5, 0.5 } };


	//desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], cores[0]);
	//desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], cores[1]);
	//desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], cores[2]);
	//desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], cores[3]);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], cores[4]);
	//desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], cores[5]);
}

void desenhaCubo()
{
	GLfloat vertices[][3] = { { -0.5, -0.5, -0.5 },
	{ 0.5, -0.5, -0.5 },
	{ 0.5, 0.5, -0.5 },
	{ -0.5, 0.5, -0.5 },
	{ -0.5, -0.5, 0.5 },
	{ 0.5, -0.5, 0.5 },
	{ 0.5, 0.5, 0.5 },
	{ -0.5, 0.5, 0.5 } };

	GLfloat cores[][3] = { { 0.0, 0.0, 1.0 },
	{ 0.0, 0.0, 1.0 },
	{ 0.0, 0.0, 1.0 },
	{ 0.0, 0.0, 1.0 },
	{ 0.0, 0.0, 1.0 },
	{ 0.0, 0.0, 1.0 },
	{ 0.0, 0.0, 1.0 } };


	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], cores[0]);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], cores[1]);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], cores[2]);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], cores[3]);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], cores[4]);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], cores[5]);
}


void desenhaDama(GLUquadric *quadratic, Damas tabuleiro) {
	glColor3fv(azulClaro[0]);
	gluCylinder(quadratic, tabuleiro.dimensaoCubos / -1.8, tabuleiro.dimensaoCubos / -1.8, tabuleiro.alturaDamas, 32, 32);

	glTranslatef(0, -1.8, tabuleiro.alturaDamas);

	gluDisk(quadratic, 0.0, tabuleiro.dimensaoCubos / 3, 50, 50);

}


// ... definicao das rotinas auxiliares de desenho ...
void desenhaTabuleiro(Damas t)
{

	GLUquadric *quad;

	quad = gluNewQuadric();


	glPushMatrix();
	glTranslatef(t.x, t.y, t.dimensaoCubos / 2);

	//borda cima
	glPushMatrix();
	glTranslatef(t.dimensaoCubos*t.numCubosHorizontal /2, 0, 0);
	glScalef(t.dimensaoCubos*t.numCubosHorizontal, t.dimensaoCubos, t.dimensaoCubos);
	desenhaCuboTabuleiro(marron);
	glPopMatrix();

	//borda baixo
	glPushMatrix();
	glTranslatef(t.dimensaoCubos * t.numCubosHorizontal / 2, (t.dimensaoCubos * (t.numCubosVertical+1)) , 0);
	glScalef(t.dimensaoCubos*t.numCubosHorizontal, t.dimensaoCubos, t.dimensaoCubos);
	desenhaCuboTabuleiro(marron);
	glPopMatrix();

	//borda esquerda
	glPushMatrix();
	glTranslatef((t.dimensaoCubos/2)+t.dimensaoCubos * t.numCubosHorizontal, (t.dimensaoCubos / 2) + t.dimensaoCubos * t.numCubosVertical/2, 0);
	glScalef(t.dimensaoCubos,t.dimensaoCubos*t.numCubosHorizontal, t.dimensaoCubos);
	desenhaCuboTabuleiro(marron);
	glPopMatrix();

	//borda direita
	glPushMatrix();
	glTranslatef(-(t.dimensaoCubos / 2), (t.dimensaoCubos / 2) + t.dimensaoCubos * t.numCubosVertical / 2, 0);
	glScalef(t.dimensaoCubos, t.dimensaoCubos*t.numCubosHorizontal, t.dimensaoCubos);
	desenhaCuboTabuleiro(marron);
	glPopMatrix();

	//canto dir. sup.
	glPushMatrix();
	glTranslatef(-t.dimensaoCubos/2, 0, 0);
	glScalef(t.dimensaoCubos, t.dimensaoCubos, t.dimensaoCubos);
	desenhaCuboTabuleiro(castanho);
	glPopMatrix();

	//canto esq. sup.
	glPushMatrix();
	glTranslatef(t.dimensaoCubos *t.numCubosHorizontal+ t.dimensaoCubos / 2, 0, 0);
	glScalef(t.dimensaoCubos, t.dimensaoCubos, t.dimensaoCubos);
	desenhaCuboTabuleiro(castanho);
	glPopMatrix();

	//canto dir. inf.
	glPushMatrix();
	glTranslatef(-t.dimensaoCubos / 2, t.dimensaoCubos *(t.numCubosVertical + 1), 0);
	glScalef(t.dimensaoCubos, t.dimensaoCubos, t.dimensaoCubos);
	desenhaCuboTabuleiro(castanho);
	glPopMatrix();

	//canto esq. inf.
	glPushMatrix();
	glTranslatef(t.dimensaoCubos *t.numCubosHorizontal + t.dimensaoCubos / 2, t.dimensaoCubos *(t.numCubosVertical+1), 0);
	glScalef(t.dimensaoCubos, t.dimensaoCubos, t.dimensaoCubos);
	desenhaCuboTabuleiro(castanho);
	glPopMatrix();

	glTranslatef(t.dimensaoCubos/2, t.dimensaoCubos, 0);

	t.numCubosHorizontal = 8;
	bool cor = false;
	for (int i = 0; i < t.numCubosHorizontal; i++)
	{
		for (int j = 0; j < t.numCubosVertical; j++)
		{
			glPushMatrix();
			glTranslatef(t.dimensaoCubos*i, t.dimensaoCubos*j, 0);

			if ((j == 0 || j == 1 || j == 2) && cor==false) {
				glPushMatrix();
					glTranslatef(t.dimensaoCubos / 2, t.dimensaoCubos / 2, t.dimensaoCubos);
					desenhaDama(quad, t);
				glPopMatrix();
			}

			if ((j == t.numCubosVertical - 3 || j == t.numCubosVertical - 2 || j == t.numCubosVertical - 1) && cor == true) {
				glPushMatrix();
				glTranslatef(t.dimensaoCubos / 2, t.dimensaoCubos / 2, t.dimensaoCubos);
				desenhaDama(quad, t);
				glPopMatrix();
			}

			glScalef(t.dimensaoCubos, t.dimensaoCubos, t.dimensaoCubos);
			if (cor == false) {
				desenhaCuboTabuleiro(castanhoEscuro); //chao
				cor = true;
			}
			else {
				desenhaCuboTabuleiro(beje); //chao 
				cor = false;
			}
			
			glPopMatrix();
		}
		if (cor) cor = false;
		else cor = true;
	}

	/*glPushMatrix();
	glScalef(t.dimensaoCubos, t.dimensaoCubos, t.dimensaoCubos);
	desenhaCubo();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(t.dimensaoCubos, 0, 0);
	glScalef(t.dimensaoCubos, t.dimensaoCubos, t.dimensaoCubos);
	desenhaCubo();
	glPopMatrix();*/


	glPopMatrix();

}


void desenhaChao(GLfloat dimensao)
{

	glBegin(GL_POLYGON);
	glVertex3f(dimensao, dimensao, 0);
	glVertex3f(-dimensao, dimensao, 0);
	glVertex3f(-dimensao, -dimensao, 0);
	glVertex3f(dimensao, -dimensao, 0);
	glEnd();

}

/*
void Timer(int value){
	glutTimerFunc(estado.delayMovimento, Timer, 0);

	if (estado.teclas.right){
		estado.camera.eye.y += 1;
	}
	if (estado.teclas.left){
		estado.camera.eye.y -= 1;
	}
	if (estado.teclas.up){
		estado.camera.eye.z += 1;
	}
	if (estado.teclas.down){
		estado.camera.eye.z -= 1;
	}
	if (estado.teclas.i){
		estado.camera.eye.z += 1;
	}
	if (estado.teclas.o){
		estado.camera.eye.z -= 1;
	}

	glutPostRedisplay();
}
*/
// Callback de desenho

void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	gluLookAt(estado.camera.eye.x, estado.camera.eye.y, estado.camera.eye.z, \
		estado.camera.center.x, estado.camera.center.y, estado.camera.center.z, \
		estado.camera.up.x, estado.camera.up.y, estado.camera.up.z);

	// ... chamada das rotinas auxiliares de desenho ...


	glColor3f(0.5f, 0.5f, 0.5f);
	desenhaChao(RAIO_ROTACAO + 5);

	glPushMatrix();


	tabuleiro.x = -10;
	tabuleiro.y = -10;
	tabuleiro.dimensaoCubos = 3;
	tabuleiro.numCubosHorizontal = 8;
	tabuleiro.numCubosVertical = 8;
	tabuleiro.alturaDamas = 0.2;
	desenhaTabuleiro(tabuleiro);

	glPopMatrix();


	glFlush();
	
}

void SpecialKey(int key, int x, int y)
{
	// ... accoes sobre outras teclas especiais ... 
	//    GLUT_KEY_F1 ... GLUT_KEY_F12
	//    GLUT_KEY_UP
	//    GLUT_KEY_DOWN
	//    GLUT_KEY_LEFT
	//    GLUT_KEY_RIGHT
	//    GLUT_KEY_PAGE_UP
	//    GLUT_KEY_PAGE_DOWN
	//    GLUT_KEY_HOME
	//    GLUT_KEY_END
	//    GLUT_KEY_INSERT 

	switch (key) {
	case GLUT_KEY_RIGHT:
		estado.teclas.right = GL_TRUE;
		break;
	case GLUT_KEY_LEFT:
		estado.teclas.left = GL_TRUE;
		break;
	case GLUT_KEY_UP:
		estado.teclas.up = GL_TRUE;
		break;
	case GLUT_KEY_DOWN:
		estado.teclas.down = GL_TRUE;
		break;
	

		// redesenhar o ecra 
		//glutPostRedisplay();
	}


	if (estado.debug)
		printf("Carregou na tecla especial %d\n", key);
}


void SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_RIGHT:
		estado.teclas.right = GL_FALSE;
		break;
	case GLUT_KEY_LEFT:
		estado.teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_UP:
		estado.teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN:
		estado.teclas.down = GL_FALSE;
		break;

	}
	if (estado.debug)
		printf("Largou a tecla especial %d\n", key);

}

void Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 27:
		exit(1);
		// ... accoes sobre outras teclas ... 

	case 'i':
	case 'I':
		estado.teclas.i = GL_TRUE;
		break;
	case 'o':
	case 'O':
		estado.teclas.o = GL_TRUE;
		break;
	}

	if (estado.debug)
		printf("Carregou na tecla %c\n", key);

}

// Callback para interaccao via teclado (largar a tecla)


void KeyUp(unsigned char key, int x, int y)
{
	switch (key) {
		// ... accoes sobre largar teclas ... 

	case 'I':
	case 'i': estado.teclas.i = GL_FALSE;
		break;
	case 'O':
	case 'o': estado.teclas.o = GL_FALSE;
		break;
	}

	if (estado.debug)
		printf("Largou a tecla %c\n", key);
}


// handles mouse click events
// button will say which button is presed, e.g. GLUT_LEFT_BUTTON, GLUT_RIGHT_BUTTON
// state will say if the button is GLUT_UP or GLUT_DOWN
// x and y are the poitner position 
void mouse_click(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		std::cerr << "\t left mouse button pressed!" << std::endl;

		if (state == GLUT_UP)
		{
			std::cerr << "\t button released...click finished" << std::endl;

			g_cursor_x = x;
			g_cursor_y = y;

			std::cerr << "\t cursor at (" << g_cursor_x << ", " <<
				g_cursor_y << ")" << std::endl;
		}

	}
	else
		if (button == GLUT_RIGHT_BUTTON)
		{
			std::cerr << "\t right mouse button pressed!" << std::endl;
		}

}

void mouse_motion(int x, int y)
{
	
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
	estado.doubleBuffer = GL_TRUE;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(640, 480);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	if (glutCreateWindow("Exemplo") == GL_FALSE)
		exit(1);

	Init();
	
	// callbacks de janelas/desenho
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	// Callbacks de teclado
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	//glutTimerFunc(estado.delayMovimento, Timer, 0);

	// mouse event handlers
	glutMouseFunc(mouse_click);
	glutPassiveMotionFunc(mouse_motion);

	// COMECAR...
	glutMainLoop();
	return 0;
}